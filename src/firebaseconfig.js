import * as firebase from 'firebase/app';
import 'firebase/firebase-firestore';

let config = {
  apiKey: "AIzaSyAruP1ZIEPho6u32oA42ANekZSRDrFOfQU",
  authDomain: "band-creator-6ebf4.firebaseapp.com",
  databaseURL: "https://band-creator-6ebf4.firebaseio.com",
  projectId: "band-creator-6ebf4",
  storageBucket: "band-creator-6ebf4.appspot.com",
  messagingSenderId: "374433131610",
  appId: "1:374433131610:web:442413fffb5f70ae2abc63",
  measurementId: "G-38703XQ095"
}

firebase.initializeApp(config);

const db = firebase.firestore()

export { db }